﻿using System.Diagnostics.Tracing;
using System.Runtime.CompilerServices;

namespace FastAtm
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int balance = 1500;
            
            Console.WriteLine("Welcome to FastAtm");
            Console.WriteLine("Please enter UserName");
            string name = Console.ReadLine();
           
            Console.WriteLine("Hello " + name + "!");
       

            Console.WriteLine("Please create a pin");
            int pin = 0;
            string pinstr = "";
           while (true) 
            {
                 pinstr = Console.ReadLine(); 
              
               
                if (pinstr.Length != 4)
                {
                    Console.WriteLine("Invalid pin digits!");
                    Console.WriteLine("please enter 4 digits only!");
                }

                else
                {
                   if ( pinstr.All(char.IsDigit)) 
                    {
                        break;
                    }
                    
                   else { Console.WriteLine("Invalid pin!");
                          Console.WriteLine("Please enter digits only!");

                    }
                }   
            }
            pin = int.Parse(pinstr);
                 Console.WriteLine("Pin created succesfully!");

            int Choice = 0;
            
            while (Choice != 6)
            {
                
                Console.WriteLine("1. Show Balance");
                Console.WriteLine("2. Withdraw funds");
                Console.WriteLine("3. Add funds");
                Console.WriteLine("4. Change pin");
                Console.WriteLine("5. Help");
                Console.WriteLine("6. Exit");
              

                 Choice = Convert.ToInt32(Console.ReadLine());





                if (Choice == 1)
                {
                    Console.WriteLine("Enter PIN to show balance...");

                    int PinInput = Convert.ToInt32(Console.ReadLine());
                    if (PinInput == pin)
                    {
                        Console.WriteLine("Login Succesfull!");
                        Console.WriteLine("Your Balance: $" + balance);

                    }
                    if (PinInput != pin)
                    {
                        Console.WriteLine("Incorrect Pin!, Try again!");
                    }
                    else if (Choice != 1)
                    {
                        break;
                    }


                }

                else if (Choice == 2)
                {
                    Console.WriteLine("Enter PIN to withdraw funds...");

                    int PinInput = Convert.ToInt32(Console.ReadLine());

                   

                    if (PinInput == pin)
                    {
                        Console.WriteLine("Login Succesfull!");
                        Console.WriteLine("Your Balance: $" + balance);
                        Console.WriteLine("Enter amount to withdraw: ");
                        
                        int Amount = Convert.ToInt32(Console.ReadLine());

                        if (Amount < balance)
                        {
                            Console.WriteLine("Removed $" + Amount);
                            balance -= Amount;
                            Console.WriteLine("Your Balance: $" + balance);
                        }

                        else if (Amount > balance)
                        {
                            Console.WriteLine("Ouuuu! I think you might be broke buddy!");
                        }
                        
                        if(PinInput != pin)
                        {
                            Console.WriteLine("Incorrect Pin!, Try again!");
                        }
                    }
                    else if (Choice != 2)
                    {
                        break;
                    }



                }
                else if (Choice == 3)
                {
                    Console.WriteLine("Enter PIN to add funds..");

                    int PinInput = Convert.ToInt32(Console.ReadLine());

                    if (PinInput == pin)
                    {
                        Console.WriteLine("Login Succesfull!");
                        Console.WriteLine("Your Balance: $" + balance);
                        Console.WriteLine("Please add Amount");
                        int AmountToAdd = Convert.ToInt32(Console.ReadLine());
                        balance += AmountToAdd;
                        Console.WriteLine("Deposit Succesfull");
                        Console.WriteLine("Your Balance: $" + balance);

                    }

                    if (PinInput != pin)
                    {
                        Console.WriteLine("Incorrect Pin!, Try again!");
                    }
                    else if (Choice != 3)
                    {
                        break;
                    }

                }

                else if (Choice == 4)
                {
                    Console.WriteLine("Enter PIN to continue..");

                    int PinInput = Convert.ToInt32(Console.ReadLine());

                    if (PinInput == pin)
                    {
                        Console.WriteLine("Login Succesfull!");
                        Console.WriteLine("Enter new PIN: ");

                        int NewPin = Convert.ToInt32(Console.ReadLine());

                        Console.WriteLine("Changed PIN from:" + pin + "TO" + NewPin);
                        pin = NewPin;
                        
                    }

                    
                    else if (Choice != 4)
                    {
                        break;
                    }

                }

                else if (Choice == 5)
                {
                    Console.WriteLine("We'll get back to you in the nearest time.");
                    
                }



                if (Choice == 6)
                {
                    Console.WriteLine("Thank you, have a nice day " + name + "!");

                    Environment.Exit(1);


                    
                }

                


            }




        }

    }
}